export default class Loader {
  constructor() {
    this.divWrapperLoader = document.createElement("div");
  }
  render(container) {
    container.append(this.divWrapperLoader);
    this.divWrapperLoader.classList.add("circle-container");
    this.divWrapperLoader.insertAdjacentHTML(
      "beforeend",
      `
              <span class="circle"></span>
              <span class="circle"></span>
              <span class="circle"></span>
              <span class="circle"></span>
          `
    );
  }

  remove() {
    const loader = document.querySelector(".circle-container");
    loader.remove();
  }
}
