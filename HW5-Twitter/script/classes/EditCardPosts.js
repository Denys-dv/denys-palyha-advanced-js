import FormModale from "./FormModal.js";
import { editCard } from "../api/api.js";

export default class EditCardPosts extends FormModale {
  constructor(title, body, editCardNewText) {
    super();
    this.title = title;
    this.body = body;
    this.editCardNewText = editCardNewText;
    this.editTextButton = document.createElement("button");
  }

  createForm() {
    super.createForm();
    this.editTextButton.setAttribute("type", "submit");
    this.editTextButton.classList.add("button__add-info");
    this.editTextButton.innerText = "Edit card";
    this.form.append(this.editTextButton);

    this.inputTitle.value = this.title;
    this.textArea.value = this.body;
  }

  render(id) {
    super.render();
    this.divModalBacground.addEventListener(
      "click",
      this.closeModale.bind(this)
    );
    this.editTextButton.addEventListener("click", async (e) => {
      e.preventDefault();
      const {
        data: { title, body },
      } = await editCard(id, this.inputTitle.value, this.textArea.value);

      this.editCardNewText(body, title);

      this.closeModale();
    });
  }
}
