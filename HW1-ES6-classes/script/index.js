class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }

  get name() {
    return this._name;
  }

  set name(value) {
    return (this._name = value);
  }

  get age() {
    return this._age;
  }

  set age(value) {
    return (this._age = value);
  }

  get salary() {
    return this._salary;
  }

  set salary(value) {
    return (this._salary = value);
  }
}

const employee = new Employee("Boby", 28, 1000);

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }

  get salary() {
    return super.salary * 3;
  }
}

const programmer = new Programmer("Denys", 30, 1500, "en");
console.log(programmer);

const programmer1 = new Programmer("Artem", 25, 3000, "ua");
console.log(programmer1);
