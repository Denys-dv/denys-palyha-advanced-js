import EditCardPosts from "../classes/EditCardPosts.js";

export function editCardUserfn(id) {
  const editCardNewText = (body, title) => {
    this.cardTitle.innerText = title;
    this.cardDescription.innerText = body;
  };

  new EditCardPosts(this.title, this.body, editCardNewText).render(id);
}
