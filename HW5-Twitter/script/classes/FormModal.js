export default class FormModale {
  constructor() {
    this.divModale = document.createElement("div");
    this.divModalBacground = document.createElement("div");
    this.divModalContainer = document.createElement("div");
    this.form = document.createElement("form");
    this.inputTitle = document.createElement("input");
    this.textArea = document.createElement("textarea");
  }

  createForm() {
    const lableTitle = document.createElement("label");
    const lableDesc = document.createElement("label");

    this.divModale.classList.add("modal");
    this.divModalBacground.classList.add("modal__background");
    this.divModalContainer.classList.add("modal__main-container");
    this.form.classList.add("form");
    this.inputTitle.classList.add("input__title");
    this.textArea.classList.add("textarea__description");

    this.inputTitle.setAttribute("type", "text");
    this.textArea.setAttribute("cols", 50);
    this.textArea.setAttribute("rows", 10);

    lableTitle.innerText = "Title";
    lableDesc.innerText = "Description";

    this.divModale.append(this.divModalBacground, this.divModalContainer);
    this.divModalContainer.append(this.form);
    this.form.append(lableTitle, lableDesc);
    lableTitle.append(this.inputTitle);
    lableDesc.append(this.textArea);
  }

  closeModale() {
    this.divModale.remove();
  }

  render(container = document.body) {
    this.createForm();
    container.prepend(this.divModale);
  }
}
