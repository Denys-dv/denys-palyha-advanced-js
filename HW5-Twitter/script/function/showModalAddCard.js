import AddCard from "../classes/AddCard.js";

export function showModalAddCard() {
  new AddCard().render();
}
