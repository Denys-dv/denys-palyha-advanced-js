const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

const root = document.getElementById("root");

class BooksErrorNotItems extends Error {
  constructor(value) {
    super();
    this.name = "BooksErrorNotItems";
    this.message = `Your object has no property ${value}`;
  }
}

class ShowBooks {
  constructor({ author, name, price }) {
    if (price === undefined) {
      throw new BooksErrorNotItems("price");
    }
    if (author === undefined) {
      throw new BooksErrorNotItems("autor");
    }
    if (name === undefined) {
      throw new BooksErrorNotItems("name");
    }

    this.author = author;
    this.name = name;
    this.price = price;

    this.listBooks = document.createElement("ul");
  }

  createElementList(root) {
    this.listBooks.classList.add("list");
    root.append(this.listBooks);
  }

  render(root) {
    this.createElementList(root);
    this.listBooks.insertAdjacentHTML(
      "beforeend",
      `
        <li class="list-items">Author: ${this.author}</li>
        <li class="list-items">Name: ${this.name}</li>
        <li class="list-items">Price: ${this.price}</li>
    `
    );
  }
}

books.forEach((el) => {
  try {
    new ShowBooks(el).render(root);
  } catch (err) {
    if (err.name === "BooksErrorNotItems") {
      console.warn(err);
    } else {
      throw err;
    }
  }
});
