import showUsersCard, { container } from "./function/showUsersCard.js";
import Header, { elementHeader } from "./classes/Header.js";

const root = document.querySelector("#root");

root.append(elementHeader, container);

showUsersCard();
new Header().render();
