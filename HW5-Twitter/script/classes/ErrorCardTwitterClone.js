class ErrorCardTwitterClone extends Error {
  constructor() {
    super();
    this.name = "Axios Error";
    this.message = "Network Error, Please try again later";
  }

  render(container) {
    container.insertAdjacentHTML(
      "beforeend",
      `
      <div>
          <p>${this.name}</p>
          <p>${this.message}</p>
      </div>
      `
    );
  }
}

export default ErrorCardTwitterClone;
