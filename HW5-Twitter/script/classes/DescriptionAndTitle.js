import Cards from "./Card.js";

class DescriptionAndTitle extends Cards {
  constructor(
    title,
    body,
    id,
    name,
    email,
    username,
    deleteCardUser,
    editCardUserfn
  ) {
    super(id, name, email, username, deleteCardUser, editCardUserfn);
    this.title = title;
    this.body = body;

    this.cardTitle = document.createElement("h2");
    this.cardDescription = document.createElement("p");
  }

  createCardUser(container) {
    super.createCardUser(container);
    this.cardTitle.innerText = this.title;
    this.cardDescription.innerText = this.body;

    this.cardTitle.classList.add("div__card-title");
    this.cardDescription.classList.add("div__card-description");
    this.divCard.append(this.cardTitle, this.cardDescription);
  }

  render(container) {
    super.render(container);
  }
}

export default DescriptionAndTitle;
