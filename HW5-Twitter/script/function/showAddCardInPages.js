import { getUsers, addCard, deleteCardUser } from "../api/api.js";
import DescriptionAndTitle from "../classes/DescriptionAndTitle.js";
import { editCardUserfn } from "./editCard.js";
import { container } from "./showUsersCard.js";

export default async function showAddCardInPages(title, body) {
  const dataUsers = await getUsers();
  const id = await addCard();

  const { name, email, username } = dataUsers[0];

  new DescriptionAndTitle(
    title,
    body,
    id,
    name,
    email,
    username,
    deleteCardUser,
    editCardUserfn
  ).render(container);
}
