import { showModalAddCard } from "../function/showModalAddCard.js";

export const elementHeader = document.createElement("header");
elementHeader.classList.add("header");

export default class Header {
  constructor() {
    this.addButtonCard = document.createElement("button");
  }

  render() {
    this.addButtonCard.classList.add("button__add-card");
    this.addButtonCard.innerText = "Add card";
    elementHeader.append(this.addButtonCard);

    this.addButtonCard.addEventListener("click", showModalAddCard);
  }
}
