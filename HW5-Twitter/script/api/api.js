import ErrorCardTwitterClone from "../classes/ErrorCardTwitterClone.js";
import { container } from "../function/showUsersCard.js";

const usersUrl = "https://ajax.test-danit.com/api/json/users";
const postsUserUrl = "https://ajax.test-danit.com/api/json/posts";

async function getUsers() {
  try {
    const { status: statusUsers, data: dataUsers } = await axios.get(usersUrl);

    if (statusUsers === 200) {
      return dataUsers;
    }
  } catch (err) {
    new ErrorCardTwitterClone().render(container);
  }
}

async function getPosts() {
  try {
    const { status: statusPost, data: postsData } = await axios.get(
      postsUserUrl
    );

    if (statusPost === 200) {
      return postsData;
    }
  } catch (err) {
    new ErrorCardTwitterClone().render(container);
  }
}

async function deleteCardUser(id) {
  try {
    if (id <= 100) {
      const { status } = await axios.delete(
        `https://ajax.test-danit.com/api/json/posts/${id}`
      );
      if (status === 200) {
        const cardFromDelet = document.getElementById(id);
        cardFromDelet.remove();
      }
    } else {
      const cardFromDelet = document.getElementById(id);
      cardFromDelet.remove();
    }
  } catch (err) {
    alert(err.message);
  }
}

async function addCard(title, body) {
  try {
    const { status, id } = await axios.post(
      "https://ajax.test-danit.com/api/json/posts",
      {
        title,
        dody: body,
      }
    );

    if (status === 200) {
      return id;
    }
  } catch (err) {
    console.log(err);
  }
}

async function editCard(id, title, body) {
  try {
    const resultEdit = await axios.put(
      `https://ajax.test-danit.com/api/json/posts/${id}`,
      {
        title,
        body: body,
      }
    );

    return resultEdit;
  } catch (err) {
    console.warn(err);
  }
}

export { getUsers, getPosts, deleteCardUser, addCard, editCard };
