export default class Cards {
  constructor(id, name, email, username, deleteCardUser, editCardUserfn) {
    this.id = id;
    this.fullName = name;
    this.email = email;
    this.username = username;

    this.deleteCardUser = deleteCardUser;
    this.editCardUserfn = editCardUserfn;
    this.divCard = document.createElement("div");
    this.buttonDeletPost = document.createElement("button");
    this.buttonEditCard = document.createElement("button");
  }

  createCardUser(container) {
    const divCardWrapper = document.createElement("div");
    const divCardWrapperInfo = document.createElement("div");
    const fullNameElement = document.createElement("p");
    const userNameElement = document.createElement("a");

    this.divCard.classList.add("div__card");
    divCardWrapper.classList.add("div__card-wrapper");
    divCardWrapperInfo.classList.add("div__card-name-info");
    fullNameElement.classList.add("div__card-full-name");
    userNameElement.classList.add("div__card-user-name");
    this.buttonDeletPost.classList.add("div__card-btn-del");
    this.buttonEditCard.classList.add("button__card-edit");

    this.divCard.setAttribute("id", `${this.id}`);
    fullNameElement.innerText = this.fullName;
    userNameElement.innerText = `@${this.username}`;
    userNameElement.setAttribute("href", `mailto:${this.email}`);
    this.buttonEditCard.innerText = "Edit card";

    divCardWrapper.append(
      divCardWrapperInfo,
      this.buttonEditCard,
      this.buttonDeletPost
    );
    divCardWrapperInfo.append(fullNameElement, userNameElement);
    this.divCard.append(divCardWrapper);
    container.prepend(this.divCard);
  }

  render(container) {
    this.createCardUser(container);

    this.buttonDeletPost.addEventListener("click", () => {
      this.deleteCardUser(this.id);
    });

    this.buttonEditCard.addEventListener("click", () => {
      this.editCardUserfn(this.id);
    });
  }
}
