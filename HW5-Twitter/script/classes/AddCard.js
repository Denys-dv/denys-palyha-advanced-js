import { addCard } from "../api/api.js";
import FormModale from "./FormModal.js";
import showAddCardInPages from "../function/showAddCardInPages.js";

class AddCard extends FormModale {
  constructor() {
    super();
    this.buttonAdd = document.createElement("button");
  }

  createForm() {
    super.createForm();
    this.buttonAdd.classList.add("button__add-info");
    this.buttonAdd.innerText = "Add";
    this.buttonAdd.setAttribute("type", "submit");

    this.form.append(this.buttonAdd);
  }

  render() {
    super.render();
    this.divModalBacground.addEventListener(
      "click",
      this.closeModale.bind(this)
    );

    this.buttonAdd.addEventListener("click", (e) => {
      e.preventDefault();
      addCard(this.inputTitle.value, this.textArea.value);
      showAddCardInPages(this.inputTitle.value, this.textArea.value);
      this.closeModale();
    });
  }
}

export default AddCard;
