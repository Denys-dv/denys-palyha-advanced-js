const butonShearch = document.querySelector(".btn");
const root = document.querySelector("#root");

butonShearch.addEventListener("click", shearchIpAndAdres);

async function shearchIpAndAdres() {
  try {
    const {
      data: { ip },
    } = await axios.get("https://api.ipify.org/?format=json");

    const {
      status,
      data: { continent, country, region, city, district },
    } = await axios.get(
      `http://ip-api.com/json/${ip}?fields=status,message,continent,country,countryCode,region,regionName,city,district,zip,lat,lon,timezone,isp,org,as,query`
    );

    if (status === 200) {
      if (district === "") {
        const district = "Area not found";
        showCardInfo(continent, country, region, city, district);
      } else {
        showCardInfo(continent, country, region, city, district);
      }
    }
  } catch (err) {
    console.warn(err);
  }
}

function showCardInfo(continent, country, region, city, district) {
  root.innerHTML = `
    <div class="container">
        <ul class="list">
            <li class="item">Континент: ${continent}</li>
            <li class="item">Країна:  ${country}</li>
            <li class="item">Регіон: ${region}</li>
            <li class="item">Місто: ${city}</li>
            <li class="item">Район: ${district}</li>
        </ul>
    <div>
  `;
}
