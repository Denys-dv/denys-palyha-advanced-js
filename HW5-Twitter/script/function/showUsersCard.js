import { getUsers, getPosts, deleteCardUser } from "../api/api.js";
import DescriptionAndTitle from "../classes/DescriptionAndTitle.js";
import Loader from "../classes/Loader.js";
import { editCardUserfn } from "./editCard.js";

export const container = document.createElement("div");
container.classList.add("container");

new Loader().render(container);

export default async function showUsersCard() {
  const dataUsers = await getUsers();
  const postsData = await getPosts();

  new Loader().remove();

  postsData.forEach(({ id, userId: userIdPosts, title, body }) => {
    const { name, email, username } = dataUsers[userIdPosts - 1];
    new DescriptionAndTitle(
      title,
      body,
      id,
      name,
      email,
      username,
      deleteCardUser,
      editCardUserfn
    ).render(container);
  });
}
