import { characters } from "../utils/characters.js";
import { satoshi2020, satoshi2019, satoshi2018 } from "../utils/exercise№4.js";

// Завдання №1

const clients1 = [
  "Гилберт",
  "Сальваторе",
  "Пирс",
  "Соммерс",
  "Форбс",
  "Донован",
  "Беннет",
];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

const newClients = (...rest) => {
  return rest.filter((elem, index, array) => array.indexOf(elem) === index);
};

console.log(newClients(...clients1, ...clients2));

// Завдання №2

const newCharacters = ({ name, lastName, age }) => {
  return { name, lastName, age };
};

const charactersShortInfo = characters.map(newCharacters);

console.log(charactersShortInfo);

// Завдання №3

const user1 = {
  name: "John",
  years: 30,
};

const { name, years: age, isAdmin = false } = user1;
console.log(name);
console.log(age);
console.log(isAdmin);

// Завдання №4

const newSatoshiFullInfo = { ...satoshi2018, ...satoshi2019, ...satoshi2020 };

console.log(newSatoshiFullInfo);

// Завдання №5

const books = [
  {
    name: "Harry Potter",
    author: "J.K. Rowling",
  },
  {
    name: "Lord of the rings",
    author: "J.R.R. Tolkien",
  },
  {
    name: "The witcher",
    author: "Andrzej Sapkowski",
  },
];

const bookToAdd = {
  name: "Game of thrones",
  author: "George R. R. Martin",
};

const newBooks = [...books, bookToAdd];
console.log(newBooks);
console.log(books);

// Завдання №6

const employee = {
  name: "Vitalii",
  surname: "Klichko",
};

const newEmployee = { age: 35, salary: 1000, ...employee };
console.log(newEmployee);
console.log(employee);

// const newEmployee = ({ name, surname, age = 35, salary = 1000 }) => {
//   return { name, surname, age, salary };
// };

// console.log(newEmployee(employee));
// console.log(employee);

// Завдання №7

const array = ["value", () => "showValue"];

const [value, showValue] = array;

alert(value); // має бути виведено 'value'
alert(showValue()); // має бути виведено 'showValue'
